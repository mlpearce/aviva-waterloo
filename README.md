# Aviva Waterloo Twitter voting application

This is a java application for receiving input from a couple of USB buttons,
and sending a message to Twitter with a hashtag according to which button
was pushed.

It was used in an installation to vote for which groups of drivers are safest,
in a publicity event for [Aviva](http://www.aviva.co.uk/) insurance. A
write-up of the event is available here: 
[Aviva Asked Waterloo Who They Thought Were Better Drivers in Experiential Campaign](http://pioneeringooh.com/aviva-asked-waterloo-who-they-thought-were-better-drivers-in-experiential-campaign/)


## Building

To build the application, use Maven:

	mvn clean package
	

## Running the application

To run the application, use the following (from the project root directory):

	java -jar target/jfx/app/waterloo-1.0-SNAPSHOT-jfx.jar config/waterloo.yml
	
The `waterloo.yml` file contains an example configuration for the application,
including the location of the Twitter authentication details, and the hashtags
available to send.

When run, the application will bring up a window asking the user to press each
button in turn, before asking which hashtags should be used to transmit. Extra
hashtags can be added from this dialog, although they are not written out to
the configuration file. The configuration file may, of course, be modified to
add new tags permanently.
