/**
 * Copyright (c) 2015 Lemur Consulting Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.co.elysian.waterloo.twitter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.PropertyConfiguration;
import uk.co.elysian.waterloo.config.TwitterConfiguration;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class to transmit Twitter messages when the button listener
 * sends them.
 *
 * Created by mlp on 23/12/15.
 */
public class TwitterTransmitter {

	private static final Logger LOGGER = LoggerFactory.getLogger(TwitterTransmitter.class);
	private static final DateFormat TWEET_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy 'at' HH:mm:ss");

	private final TwitterConfiguration config;
	private final Twitter twitter;

	private int voteCount;

	public TwitterTransmitter(TwitterConfiguration config) throws IOException {
		this.config = config;
		Configuration twitterConfig = new PropertyConfiguration(new FileInputStream(config.getAuthConfigFile()));
		twitter = new TwitterFactory(twitterConfig).getInstance();
	}

	public void sendMessage(String hashtag) throws TwitterException {
		StringBuilder msg = new StringBuilder("Vote number ");
		msg.append(voteCount ++);
		msg.append(" on ");
		msg.append(TWEET_DATE_FORMAT.format(new Date()));
		msg.append(" ");
		msg.append(hashtag);

		if (config.isEnabled()) {
			twitter.tweets().updateStatus(msg.toString());
		}
		LOGGER.debug("Sent message [{}]", msg.toString());
	}

}
