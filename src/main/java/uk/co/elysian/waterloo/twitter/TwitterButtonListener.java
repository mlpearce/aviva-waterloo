/**
 * Copyright (c) 2015 Lemur Consulting Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.co.elysian.waterloo.twitter;

import javafx.application.Platform;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import org.apache.commons.lang3.StringUtils;
import org.hid4java.HidDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;
import uk.co.elysian.waterloo.button.ButtonEvent;
import uk.co.elysian.waterloo.button.ButtonListener;

import java.util.List;

/**
 * The class to fire Twitter messages when a button is pressed.
 * <p/>
 * Created by mlp on 22/12/15.
 */
public class TwitterButtonListener implements ButtonListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(TwitterButtonListener.class);

	private final TwitterTransmitter transmitter;
	private final Media succesMedia;

	private String[] buttonPaths;

	private List<String> values;

	public TwitterButtonListener(TwitterTransmitter tt, Media successMedia) {
		this.transmitter = tt;
		this.succesMedia = successMedia;
	}

	public void setButtonPaths(String button1, String button2) {
		buttonPaths = new String[]{ button1, button2 };
	}

	@Override
	public void handleButtonEvent(ButtonEvent event, HidDevice button) {
		if (event == ButtonEvent.BUTTON_PRESSED) {
			int idx = findButtonIndex(button.getPath());
			if (idx == -1) {
				LOGGER.warn("Unrecognized button: {}", button.getPath());
			} else {
				sendTwitterMessage(idx);
			}
		}
	}

	private int findButtonIndex(String path) {
		int idx = -1;
		for (int i = 0; i < buttonPaths.length; i++) {
			if (path.equals(buttonPaths[i])) {
				idx = i;
				break;
			}
		}

		return idx;
	}

	private void sendTwitterMessage(int buttonIdx) {
		LOGGER.debug("Sending message for button {}", buttonIdx);
		if (values == null || StringUtils.isBlank(values.get(buttonIdx))) {
			LOGGER.warn("No value set for button {}", buttonIdx);
		} else {
			try {
				String hashtag = values.get(buttonIdx);
				transmitter.sendMessage(hashtag);

				// Beep-boop!
				Platform.runLater(() -> {
					MediaPlayer mp = new MediaPlayer(succesMedia);
					mp.play();
				});
			} catch (TwitterException e) {
				LOGGER.error("Could not send message to Twitter: {}", e.getMessage());
			}
		}
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

}
