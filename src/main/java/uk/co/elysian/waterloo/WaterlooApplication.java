/**
 * Copyright (c) 2015 Lemur Consulting Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.co.elysian.waterloo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.media.Media;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.elysian.waterloo.button.USBButtonHidServicesListener;
import uk.co.elysian.waterloo.config.WaterlooConfiguration;
import uk.co.elysian.waterloo.config.loaders.ConfigurationLoaderFactory;
import uk.co.elysian.waterloo.twitter.TwitterButtonListener;
import uk.co.elysian.waterloo.twitter.TwitterTransmitter;
import uk.co.elysian.waterloo.view.ButtonPaneController;
import uk.co.elysian.waterloo.view.TwitterMessagePaneController;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Central application for the Aviva Waterloo button project.
 *
 * Created by mlp on 21/12/15.
 */
public class WaterlooApplication extends Application {

	private static final Logger LOGGER = LoggerFactory.getLogger(WaterlooApplication.class);

	private static final String SUCCESS_MP3 = "/Boxing_arena_sound-Samantha_Enrico-246597508.mp3";

	private WaterlooConfiguration configuration;

	private final USBButtonHidServicesListener buttonListener;

	private Stage primaryStage;
	private BorderPane rootLayout;

	private GridPane buttonPane;
	private ButtonPaneController buttonPaneController;

	private GridPane messagePane;
	private TwitterMessagePaneController messagePaneController;

	private TwitterTransmitter twitterTransmitter;
	private TwitterButtonListener twitterButton;
	private Media successMedia;

	public WaterlooApplication() {
		buttonListener = new USBButtonHidServicesListener();
	}

	@Override
	public void init() {
		// Load the configuration
		List<String> args = getParameters().getRaw();
		if (args.size() == 0) {
			usage();
			System.exit(1);
		} else {
			try {
				configuration = ConfigurationLoaderFactory.buildConfigurationLoader(args.get(args.size() - 1)).loadConfiguration();
				twitterTransmitter = new TwitterTransmitter(configuration.getTwitter());

				// Initialise the success media
				successMedia = new Media(WaterlooApplication.class.getResource(SUCCESS_MP3).toURI().toString());
			} catch (IOException e) {
				LOGGER.error("Could not load configuration: {}", e.getMessage());
				System.err.println("Could not load configuration: " + e.getMessage());
				usage();
				System.exit(1);
			} catch (URISyntaxException e) {
				LOGGER.error("URI exception: {}", e.getMessage());
			} catch (IllegalArgumentException e) {
				LOGGER.error("Illegal argument in MP3 name: {}", e.getMessage());
			}
		}
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;
		primaryStage.setTitle("Aviva : Waterloo Twitter button manager");

		initRootLayout();

		displayButtonPane();
	}

	private void initRootLayout() {
		try {
			// Load root layout from FXML file
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(WaterlooApplication.class.getResource("view/RootLayout.fxml"));
			rootLayout = loader.load();

			// Set the scene
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			LOGGER.error("Exception loading root layout: {}", e.getMessage());
		}
	}

	private void displayButtonPane() {
		try {
			FXMLLoader loader = new FXMLLoader(WaterlooApplication.class.getResource("view/ButtonPane.fxml"));
			buttonPane = loader.load();

			// Add to the root layout
			rootLayout.setCenter(buttonPane);

			buttonPaneController = loader.getController();
			buttonListener.registerButtonListener(buttonPaneController);

			buttonPaneController.getInitialised().addListener((observable, oldValue, newValue) -> {
				if (newValue) {
					// Fire up the Twitter button listener
					twitterButton = new TwitterButtonListener(twitterTransmitter, successMedia);

					String[] buttonPaths = buttonPaneController.getButtonPaths();
					twitterButton.setButtonPaths(buttonPaths[0], buttonPaths[1]);

					buttonListener.registerButtonListener(twitterButton);
					buttonListener.removeButtonListener(buttonPaneController);

					// Show the message pane
					displayMessagePane();
				}
			});
		} catch (IOException e) {
			LOGGER.error("Exception loading button pane: {}", e.getMessage());
		}
	}

	private void displayMessagePane() {
		try {
			FXMLLoader loader = new FXMLLoader(WaterlooApplication.class.getResource("view/TwitterMessagePane.fxml"));
			messagePane = loader.load();

			// Set in the root layout
			rootLayout.setCenter(messagePane);

			messagePaneController = loader.getController();
			messagePaneController.setApplication(this);
			messagePaneController.setConfiguration(configuration);
			messagePaneController.setButtonListener(twitterButton);
		} catch (IOException e) {
			LOGGER.error("Exception loading button pane: {}", e.getMessage(), e);
		}
	}

	@Override
	public void stop() throws Exception {
		super.stop();
		buttonListener.shutdown();
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public static void main(String[] args) {
		launch(args);
	}

	private static void usage() {
		System.out.println("Usage:");
		System.out.println("  java uk.co.flax.biosolr.ontology.OntologyIndexer configfile");
	}

}
