/**
 * Copyright (c) 2015 Lemur Consulting Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.co.elysian.waterloo.view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.elysian.waterloo.WaterlooApplication;
import uk.co.elysian.waterloo.config.WaterlooConfiguration;
import uk.co.elysian.waterloo.twitter.TwitterButtonListener;

import java.io.IOException;
import java.util.List;

/**
 * Controller class for the Twitter message pane.
 *
 * Created by mlp on 22/12/15.
 */
public class TwitterMessagePaneController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TwitterMessagePaneController.class);

	private WaterlooApplication application;
	private WaterlooConfiguration configuration;

	@FXML
	private Label button1Label;
	@FXML
	private TextField button1Text;

	@FXML
	private Label button2Label;
	@FXML
	private TextField button2Text;

	@FXML
	private Button editButton;

	private TwitterButtonListener buttonListener;

	public TwitterMessagePaneController() {
	}

	@FXML
	private void initialize() {
		button1Text.setVisible(false);
		button2Text.setVisible(false);
	}

	public void setApplication(WaterlooApplication application) {
		this.application = application;
	}

	public void setConfiguration(WaterlooConfiguration config) {
		this.configuration = config;
	}

	@FXML
	private void handleEditButton() {
		try {
			FXMLLoader loader = new FXMLLoader(WaterlooApplication.class.getResource("view/MessageSelectionDialog.fxml"));
			AnchorPane pane = loader.load();

			// Create the dialog stage
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Choose hash tags");
			dialogStage.initOwner(application.getPrimaryStage());
			dialogStage.initModality(Modality.WINDOW_MODAL);
			Scene scene = new Scene(pane);
			dialogStage.setScene(scene);

			// Get the controller
			MessageSelectionDialogController controller = loader.getController();
			controller.setApplication(application);
			controller.setDialogStage(dialogStage);
			controller.setHashTagValues(configuration.getHashTags());

			if (button1Text.isVisible()) {
				controller.setSelectedValue(button1Text.getText(), button2Text.getText());
			}

			dialogStage.showAndWait();

			if (controller.isSavePressed()) {
				List<String> values = controller.getSelectedValue();
				button1Text.setText(values.get(0));
				button1Text.setVisible(true);
				button2Text.setText(values.get(1));
				button2Text.setVisible(true);

				buttonListener.setValues(values);
				if (controller.isNewTag()) {
					configuration.getHashTags().add(values);
				}
			}
		} catch (IOException e) {
			LOGGER.error("Could not load message selection dialog: {}", e.getMessage());
		}
	}

	public void setButtonListener(TwitterButtonListener listener) {
		this.buttonListener = listener;
	}

}
