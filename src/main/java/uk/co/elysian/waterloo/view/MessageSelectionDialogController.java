/**
 * Copyright (c) 2015 Lemur Consulting Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.co.elysian.waterloo.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.elysian.waterloo.WaterlooApplication;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Dialog for selecting hash tags to send.
 *
 * Created by mlp on 22/12/15.
 */
public class MessageSelectionDialogController {

	private static final String SEPARATOR = " // ";

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageSelectionDialogController.class);

	@FXML
	private ComboBox<String> hashTags;

	private ObservableList<String> hashTagValues;

	private WaterlooApplication application;
	private Stage dialogStage;

	private TwitterMessagePaneController messageController;

	private boolean savePressed;
	private boolean newTag;

	@FXML
	private void initialize() {

	}

	public void setHashTagValues(List<List<String>> values) {
		hashTagValues = FXCollections.observableArrayList();

		for (List<String> pair : values) {
			hashTagValues.add(StringUtils.join(pair, SEPARATOR));
		}

		hashTags.setItems(hashTagValues);
	}

	public void setDialogStage(Stage stage) {
		this.dialogStage = stage;
	}

	public void setApplication(WaterlooApplication app) {
		this.application = app;
	}

	public void handleSave() {
		savePressed = true;
		dialogStage.close();
	}

	public void handleCancel() {
		savePressed = false;
		dialogStage.close();
	}

	public void handleAddHashTags() {
		try {
			FXMLLoader loader = new FXMLLoader(WaterlooApplication.class.getResource("view/AddHashTagDialog.fxml"));
			AnchorPane pane = loader.load();

			// Create the dialog stage
			Stage addTagsStage = new Stage();
			addTagsStage.setTitle("Add hash tags");
			addTagsStage.initOwner(dialogStage);
			addTagsStage.initModality(Modality.WINDOW_MODAL);
			Scene scene = new Scene(pane);
			addTagsStage.setScene(scene);

			// Get the controller
			AddHashTagDialogController controller = loader.getController();
			controller.setDialogStage(addTagsStage);

			addTagsStage.showAndWait();

			if (controller.isSavePressed()) {
				hashTagValues.add(StringUtils.join(new String[]{ controller.getButton1(), controller.getButton2() }, SEPARATOR));
				setSelectedValue(controller.getButton1(), controller.getButton2());
				newTag = true;
			}
		} catch (IOException e) {
			LOGGER.error("Could not load add tags dialog: {}", e.getMessage());
		}

	}

	public boolean isSavePressed() {
		return savePressed;
	}

	public boolean isNewTag() {
		return newTag;
	}

	public List<String> getSelectedValue() {
		String value = hashTags.valueProperty().getValue();
		return Arrays.asList(value.split(SEPARATOR));
	}

	public void setSelectedValue(String btn1, String btn2) {
		String selected = StringUtils.join(new String[]{ btn1, btn2 }, SEPARATOR);
		for (String item : hashTagValues) {
			if (item.equals(selected)) {
				hashTags.valueProperty().setValue(item);
			}
		}
	}

}
