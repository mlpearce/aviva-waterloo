/**
 * Copyright (c) 2015 Lemur Consulting Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.co.elysian.waterloo.view;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.fxml.FXML;
import javafx.scene.text.Text;
import org.hid4java.HidDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.elysian.waterloo.button.ButtonEvent;
import uk.co.elysian.waterloo.button.ButtonListener;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Controller class for the button pane, displayed when the app
 * starts up, and used to determine which button is which.
 * <p/>
 * Created by mlp on 22/12/15.
 */
public class ButtonPaneController implements ButtonListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(ButtonPaneController.class);

	@FXML
	private Text buttonText;

	private Set<HidDevice> buttons = new LinkedHashSet<>();

	private IntegerProperty buttonCount = new SimpleIntegerProperty();
	private BooleanProperty initialised = new SimpleBooleanProperty(false);

	public ButtonPaneController() {

	}

	@FXML
	private void initialize() {
		buttonCount.addListener((observable, oldValue, newValue) -> {
			if (newValue.intValue() == 1) {
				Platform.runLater(() ->
						buttonText.setText("Press Button 2")
				);
			} else if (newValue.intValue() == 2) {
				Platform.runLater(() ->
						initialised.setValue(true)
				);
			}
		});
	}

	@Override
	public void handleButtonEvent(ButtonEvent event, HidDevice button) {
		if (event == ButtonEvent.BUTTON_PRESSED) {
			// Check that button not already added
			if (!buttonAlreadyPresent(button)) {
				buttons.add(button);
				buttonCount.setValue(buttons.size());
				LOGGER.info("Registered button {} at {}", buttons.size(), button.getPath());
			}
		}
	}

	private boolean buttonAlreadyPresent(HidDevice button) {
		boolean pressed = false;
		for (HidDevice btn : buttons) {
			if (btn.equals(button)) {
				LOGGER.warn("Button {} pressed more than once - skipping", button.getPath());
				pressed = true;
				break;
			}
		}
		return pressed;
	}

	public String[] getButtonPaths() {
		List<String> pathList = buttons.stream().map(HidDevice::getPath).collect(Collectors.toList());
		return pathList.toArray(new String[pathList.size()]);
	}

	public BooleanProperty getInitialised() {
		return initialised;
	}

}
