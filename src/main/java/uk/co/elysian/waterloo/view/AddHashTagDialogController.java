/**
 * Copyright (c) 2015 Lemur Consulting Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.co.elysian.waterloo.view;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller for the add hash tags dialog.
 *
 * Created by mlp on 23/12/15.
 */
public class AddHashTagDialogController {

	private boolean savePressed;

	private Stage dialogStage;

	@FXML
	private TextField button1Text;

	@FXML
	private TextField button2Text;

	public void setDialogStage(Stage stage) {
		this.dialogStage = stage;
	}

	public void handleSave() {
		savePressed = true;
		dialogStage.close();
	}

	public void handleCancel() {
		savePressed = false;
		dialogStage.close();
	}

	public String getButton1() {
		return button1Text.getText();
	}

	public String getButton2() {
		return button2Text.getText();
	}

	public boolean isSavePressed() {
		return savePressed;
	}
}
