/**
 * Copyright (c) 2015 Lemur Consulting Ltd.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.co.elysian.waterloo.button;

import org.hid4java.HidDevice;
import org.hid4java.HidManager;
import org.hid4java.HidServices;
import org.hid4java.HidServicesListener;
import org.hid4java.event.HidServicesEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Listener service to receive events from the USB buttons.
 *
 * Created by mlp on 21/12/15.
 */
public class USBButtonHidServicesListener implements HidServicesListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(USBButtonHidServicesListener.class);

	private static final int USB_BUTTON_VENDOR_ID = 0xFFFFD209;
	private static final int USB_BUTTON_PRODUCT_ID = 0x1200;
	private static final int USB_BUTTON_INPUT_INTERFACE = 1;

	private final Map<String, USBButtonListenerThread> threadMap = new HashMap<>();

	private final List<ButtonListener> listeners = new ArrayList<>();

	public USBButtonHidServicesListener() {
		HidServices hidServices = HidManager.getHidServices();
		hidServices.addHidServicesListener(this);

		LOGGER.debug("Enumerating attached devices...");

		// Provide a list of attached devices
		hidServices.getAttachedHidDevices().stream()
				.filter(USBButtonHidServicesListener::isUSBButton)
				.forEach(this::startButtonThread);

		LOGGER.info("Found {} USB buttons attached", threadMap.size());
		if (LOGGER.isTraceEnabled()) {
			for (String path : threadMap.keySet()) {
				LOGGER.trace("{}", threadMap.get(path).button);
			}
		}
	}

	@Override
	public void hidDeviceAttached(HidServicesEvent event) {
		if (isUSBButton(event.getHidDevice())) {
			startButtonThread(event.getHidDevice());
			LOGGER.info("Button attached: {}", event.getHidDevice());
		}
	}

	private static boolean isUSBButton(HidDevice device) {
		return device.getVendorId() == USB_BUTTON_VENDOR_ID
				&& device.getProductId() == USB_BUTTON_PRODUCT_ID
				&& device.getInterfaceNumber() == USB_BUTTON_INPUT_INTERFACE;
	}

	private void startButtonThread(HidDevice button) {
		if (threadMap.containsKey(button.getPath())) {
			LOGGER.warn("Duplicate thread detected for {}", button.getPath());
			stopButtonThread(button);
		}

		USBButtonListenerThread t = new USBButtonListenerThread(this, button);
		threadMap.put(button.getPath(), t);
		t.start();
		LOGGER.debug("Started listener thread for {}", button.getPath());
		sendButtonEvent(ButtonEvent.BUTTON_ATTACHED, button);
	}

	@Override
	public void hidDeviceDetached(HidServicesEvent event) {
		LOGGER.info("Device detached: {}", event);
		stopButtonThread(event.getHidDevice());
	}

	private void stopButtonThread(HidDevice button) {
		String path = button.getPath();
		if (threadMap.containsKey(path)) {
			USBButtonListenerThread t = threadMap.get(path);
			t.setRunning(false);

			while (t.isAlive()) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					LOGGER.error("Interrupted while shutting down button thread: {}", e.getMessage());
				}
			}

			sendButtonEvent(ButtonEvent.BUTTON_DETACHED, button);
			LOGGER.debug("Shut down listener thread for button {}", path);
		}
	}

	@Override
	public void hidFailure(HidServicesEvent event) {
		LOGGER.warn("HID Failure: {}", event);
	}

	public void registerButtonListener(ButtonListener listener) {
		synchronized (listeners) {
			listeners.add(listener);
		}
	}

	public void removeButtonListener(ButtonListener listener) {
		synchronized (listeners) {
			listeners.remove(listener);
		}
	}

	private void sendButtonEvent(ButtonEvent event, HidDevice button) {
		synchronized (listeners) {
			listeners.forEach(l -> l.handleButtonEvent(event, button));
		}
	}

	public void shutdown() {
		LOGGER.info("Shutting down button service listener");
		for (Iterator<Map.Entry<String, USBButtonListenerThread>> it = threadMap.entrySet().iterator(); it.hasNext(); ) {
			Map.Entry<String, USBButtonListenerThread> entry = it.next();
			USBButtonListenerThread t = entry.getValue();
			t.setRunning(false);
			while (t.isAlive()) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					LOGGER.error("Interrupted during shutdown: {}", e.getMessage());
				}
			}
			it.remove();
		}
	}


	class USBButtonListenerThread extends Thread {

		final USBButtonHidServicesListener listener;
		final HidDevice button;

		boolean running;

		USBButtonListenerThread(USBButtonHidServicesListener listener, HidDevice button) {
			this.listener = listener;
			this.button = button;
		}

		@Override
		public void run() {
			if (!button.isOpen()) {
				button.open();
			}

			// Set non-blocking
			button.setNonBlocking(true);
			byte[] packet = new byte[64];
			int val;

			running = true;
			while (running) {
				val = button.read(packet);
				if (val <= 0) {
					if (val < 0) {
						LOGGER.error("Issue with button at {}: {}", button.getPath(), button.getLastErrorMessage());
					}
				} else {
					LOGGER.debug("{}: Read {} bytes [ {} ]", button.getPath(), val, new String(packet));
					if (getByteCount(packet, val) == 1) {
						// End of broadcast - signal
						listener.sendButtonEvent(ButtonEvent.BUTTON_PRESSED, button);
						LOGGER.debug("Read complete message from {}", button.getPath());
					}
				}

				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					LOGGER.error("Thread sleep interrupted: {}", e.getMessage());
				}
			}

			// Main run loop finished - close button
			button.close();
		}

		private int getByteCount(byte[] packet, int len) {
			int count = 0;
			for (int i = 0; i < len; i ++) {
				if (packet[i] != 0) {
					count ++;
				}
			}
			return count;
		}

		public void setRunning(boolean r) {
			this.running = r;
		}

	}

}
